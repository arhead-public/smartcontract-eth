const main = async () => {
  const nftContractFactory = await hre.ethers.getContractFactory("ARHeadTestnet");
  const nftContract = await nftContractFactory.deploy();
  await nftContract.deployed();
  console.log("Contract deployed to:", nftContract.address);
  
  const [owner] = await ethers.getSigners();
  
  let txn = await nftContract.safeMint(owner.address, 199432225750057);
  await txn.wait();
  
  txn = await nftContract.burnToken(199432225750057);
  await txn.wait();
};

const runMain = async () => {
  try {
    await main();
    process.exit(0);
  } catch (error) {
    console.log(error);
    process.exit(1);
  }
};

runMain();
